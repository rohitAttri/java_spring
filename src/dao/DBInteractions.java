/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author rohit
 */
public class DBInteractions {

    public static final Logger LOGS = Logger.getLogger(DBInteractions.class.getName());
    private JdbcTemplate jdbcObjLive;
    private JdbcTemplate jdbcObjReplica2;

    public void setDataSourceLive(DataSource dataSourceLive) {
        this.jdbcObjLive = new JdbcTemplate(dataSourceLive);
    }

    public void setDataSourceReplica2(DataSource dataSourceReplica2) {
        this.jdbcObjReplica2 = new JdbcTemplate(dataSourceReplica2);
    }

    public int getLastProcessedID() {
        String sql1 = "Select last_id FROM process_info WHERE id=1";
        int id = jdbcObjReplica2.queryForObject(sql1, Integer.class);
        return id;
    }

    public List getEventData() {

        int last_id = getLastProcessedID();
        String query1 = "SELECT customer_id FROM recharge";
//        String query1 = "SELECT `id` as customer_id,`email_id`,`android_id`,`google_ad_id`,1 as type FROM `customers` WHERE `is_active`=1";
        String query = "Select A.id,A.customer_id, B.email_id, B.google_ad_id, B.android_id, A.`type` "
                + "FROM branch_events A "
                + "LEFT JOIN customers B on A.customer_id=B.id "
                + "WHERE B.is_active=1 AND A.id > " + last_id;
        LOGS.info(query1);
        List data = jdbcObjLive.queryForList(query1);
        return data;
    }
}
