/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apsalar;

import dao.DBInteractions;
import frodo.FrodoApiValidation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author rohit
 */
public class GetApsalarEvents {
    private static final Logger LOGS = Logger.getLogger(FrodoApiValidation.class.getName());
    static JSONObject failedEmails = new JSONObject();
    static JSONObject fileCount = new JSONObject();
    static int notFound = 0;
    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        AbstractApplicationContext context
                = new ClassPathXmlApplicationContext("applicationContext.xml");

        DBInteractions db = (DBInteractions) context.getBean("dbinteraction");
        int count = 1;
//        System.out.println("HELLO");
//        System.exit(0);
        try {
            List events = db.getEventData();
            int BatchSize = 50;
            int Records = events.size();
            if (Records > 0) {
                int BatchCount = Records/BatchSize;
                int RecordsLeft = Records%BatchSize;
                if(RecordsLeft > 0) {
                    BatchCount = BatchCount + 1;
                }
                int BatchNumber = 1;
                Map eventList = new HashMap();
                List cudData = new ArrayList();
                String userIds = "";
                long rowID;
//                int total=0;
    //                eventList("1", 3);
                for (Object map1 : events) {
                    Map map = (Map) map1;
//                    cudData = new ArrayList();
    //                rowID = Long.parseLong(map.get("id").toString());
                    String cid = map.get("customer_id").toString();
//                    String GID = map.get("google_ad_id").toString();
//                    String ANDI = map.get("android_id").toString();
//                    String EmailID = map.get("email_id").toString();
    //                int event = (int) map.get("type");
                    int event = 1;
                    int a=0;
                    int b=0;
//                    cudData.add(GID);
//                    cudData.add(ANDI);
//                    cudData.add(EmailID);
//                    cudData.add(event);
//                    eventList.put(cid, cudData);
                    if (count == 1) {
                        userIds = cid + "";
                    } else {
                        userIds += "," + cid;
                    }
                    if (count == BatchSize) {
                        BatchNumber++;
                        count = 0;
                        a = sendDatatoAPIs(userIds, eventList);
//                        if(BatchNumber==5) break;
                        
                    } else if((BatchCount==BatchNumber) && (count==RecordsLeft)) {
                        b = sendDatatoAPIs(userIds, eventList);
                    }
//                    total = total+(a+b);
                    count++;
                }
                System.out.println("fileIdCount:"+fileCount);
                System.out.println("FailedEamils:"+failedEmails);
                System.out.println("NotFound:"+notFound);
                cudData.removeAll(cudData);
            } else {
                LOGS.info("No Data to Process");
            }
        }catch(Exception ex) {
            LOGS.info(ex.getMessage());
        }
    }

    /**
     *
     * @param userIds
     * @param eventList
     * @return 
     */
    public static int sendDatatoAPIs(String userIds, Map eventList) {
        FrodoApiValidation frd = new FrodoApiValidation();
        JSONObject data = frd.checkWithFrodo(userIds);
        Iterator a = data.keys();
        int count=0;
        while(a.hasNext()) {
            try {
                String cusID = a.next().toString();
                JSONObject innerData = data.getJSONObject(cusID);
                if(innerData.has("found")) {
                    if(innerData.get("found").toString().equals("true")) {
                        int fileIdCount = (int) innerData.get("same_fileId_count");
                        String flag = innerData.get("email_flag").toString();
                        String email = innerData.get("email").toString();
                        String reason = innerData.get("email_flag_reason").toString();
                        if(fileIdCount > 1) fileCount.put(email, fileIdCount);
                        if(flag.equals("true")) failedEmails.put(email, reason);
//                        List userData = (List) eventList.get(cusID);
//                        String EamilID = innerData.get("email").toString();
//                        if(QuickEmail.emailValidation(EamilID)) {
//                            ApsalarApi apApi = new ApsalarApi();
//                            String apiResponse = apApi.sendToAppsalar(userData,cusID);
//                            if(apiResponse.equals("Y")) {
//                                LOGS.info(apiResponse);
//                            }
//                        } else {
//                            LOGS.info("Failed by QuickEmail:"+EamilID);
//                        }
                    }else {
                        notFound++;
                    }
                }
            } catch (JSONException ex) {
                LOGS.info(ex.getMessage());
            }
        }
        return count;
//        System.out.println("Records:"+count);
    }
}
