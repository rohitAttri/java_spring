package apsalar;


import org.apache.log4j.Logger;

public class Utility {
	private static final Logger LOG = Logger.getLogger(Utility.class.getName());

    public static boolean isNullString(String inStr){
            return null==inStr||inStr.length()==0;
    }


    public String giveRegistrationEvent(int type)
    {
        switch (type) {
            case 1:	//New Referral user register
                return "NR-DB1";
            case 2:	//New user register
                return "N-DB1";
            case 3:	//Non play store New user register
                return "N-ODB1";
            case 4:	//Guest user login
                return "N-GDB1";
            case 5:	//Referral Guest user register
                return "N-GRDB1";
            case 6:	//Non Play store Guest Login
                return "N-OGDB1";
            default:
                return "";
        }
    }
}
