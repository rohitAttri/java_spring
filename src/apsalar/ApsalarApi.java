/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apsalar;

import dao.DBInteractions;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.logging.Logger;
import javax.net.ssl.HttpsURLConnection;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author rohit
 */
public class ApsalarApi {
    
    /**
     *
     */
    public static final Logger logger = Logger.getLogger(DBInteractions.class.getName());
    private final static String API_KEY = "til_dev";
    private final static String OS = "Android";
    private final static String BASE_URL = "https://api.apsalar.com/api/v1/evt";
    private final static String APK_PACKAGE = "free.mobile.internet.data.recharge";

//    public String sendToAppsalar(AppsalarRegMaster appsalarRegMaster) {

    /**
     *
     * @param Result
     * @param event
     * @return
     */
    public String sendToAppsalar(List Result, String customerID) {
        JSONObject jsonObj = null;

        URL url = null;
        HttpsURLConnection httpsConn = null;
        BufferedReader bufferReader = null;
        String apsalarURL = "";
            String GID = Result.get(0).toString();          // GoogleAdID
            String Andi = Result.get(1).toString();         // AndroidID
            String Email = Result.get(2).toString();        // EmailID
            int event = (int) Result.get(3);        // Event
            Utility utl = new Utility();
            String finalEvent = utl.giveRegistrationEvent(event);
        try {
            jsonObj = new JSONObject();
            jsonObj.put("android_id", Andi);
            jsonObj.put("email", Email);
            jsonObj.put("customer_id", customerID);

            apsalarURL = BASE_URL;
            if ( GID != null && !GID.trim().equals("")) {
                apsalarURL = apsalarURL + "?aifa=" + GID;
            } else {
                apsalarURL = apsalarURL + "?andi=" + Andi;
            }

            apsalarURL = apsalarURL + "&p=" + OS + "&a=" + API_KEY + "&i=" + APK_PACKAGE + "&e=" + URLEncoder.encode(jsonObj.toString(), "UTF-8");

            apsalarURL = apsalarURL + "&n=" + finalEvent;

            logger.info("****Appsalar Api calling*****");
            logger.info(apsalarURL);

            url = new URL(apsalarURL);
            httpsConn = (HttpsURLConnection) url.openConnection();

            httpsConn.setRequestMethod("GET");
            httpsConn.setConnectTimeout(25000);
            httpsConn.setReadTimeout(25000);
            int responseCode = httpsConn.getResponseCode();
            logger.info("Response Code::" + responseCode);

          
            bufferReader = new BufferedReader(new InputStreamReader(httpsConn.getInputStream()));
            String inputLine = "";
            StringBuffer response = new StringBuffer();

            while ((inputLine = bufferReader.readLine()) != null) {
                response.append(inputLine);
            }
            bufferReader.close();

            logger.info("Response ::" + response);
            logger.info("****Appsalar Api End*****\n");
            return "Y";
        } catch (JSONException | IOException e) {
            logger.warning("Error Response ::"+e);
            System.out.println("Exception occured in ApsalarRegEvent(sendToAppsalar) ::" + e.getMessage());
            logger.info("****Api End*****\n");
        }
        return "F";
    }
    
}
